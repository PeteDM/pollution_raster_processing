

years <- 2001:2016

#####
# Important: the data is NOT modified
# - for PM10, 2001 to 2003 are TEOM  rather than gravimetric units. They need to be multiplied by 1.3
# - for several series, data are only available from 2002, 2003, etc. The easiest step to impute 
#   previous years is to go to the relevant Ricardo folder, duplicate the first year of data, and rename it to 2001, 2002, etc.
#####


#define data paths
shpfiles <- "~/Documents/0 Datasets/shp/"
ricardo <- "~/Documents/0 Datasets/Ricardo/"
# shpfiles <- "V:/VolumeB/APIC/pollution_data/shp/"
# ricardo <- "V:/VolumeB/APIC/pollution_data/Ricardo/"
pm25files <- paste0(ricardo, "Ricardo_PM25/")
pm10files <- paste0(ricardo, "Ricardo_PM10/")
no2files <- paste0(ricardo, "Ricardo_NO2/")
nofiles <- paste0(ricardo, "Ricardo_NO/")
benzenefiles <- paste0(ricardo, "Ricardo_Benzene/")
o3files <- paste0(ricardo, "Ricardo_O3/")
cofiles <- paste0(ricardo, "Ricardo_CO/")
comax8files <- paste0(ricardo, "Ricardo_COmax8/")
so2files <- paste0(ricardo, "Ricardo_SO2/")

#############################
# # Run once only!
# #
# # STEP 1
# # Verifying that directory exist, creating them if necessary
for(dirs in list(ricardo, pm25files, pm10files, nofiles, no2files, benzenefiles,
                 o3files, cofiles, comax8files, so2files, shpfiles)){
  if(!dir.exists(dirs)) dir.create(dirs)
}
# #
# # STEP 2
# # Installing packages and downloading shapefiles and
# # pollution data from https://uk-air.defra.gov.uk/data/pcm-data
source("dependencies/downloading_data.R")
# #
# # STEP 3
# # Joining the data in a grid per year long table
source("dependencies/loading_data_longfile.R", echo = T)
############################


# Packages
library("raster")
library("rgdal")
library("sqldf")
OSGBproj <- "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +datum=OSGB36 +units=m +vunits=m +no_defs"

# # load the pollution bricks (poldb)
load(file = "data_outputs/pollution_bricks.RData")

# # load the pollution database (pollution_db_lg)
load(file = paste0(ricardo, "pollution_database_long.RData"))


# To print a year by year overview of pollution maps
dev.off()
for(pollut in c('CO_max8hour', 'CO_mean', 'NO2_annual', 'NOx_annual',
                'O3_120microgexceed', 'PM10_annual', 'PM25_annual', 'SO2_annual')){
  pdf(paste0("graphs/", pollut, "_brick.pdf"), 
      width = .71*15*2, height=1.5*2)
  par(mar=rep(0,4))
  # tmpbrick <- brick(poldb[grep(pollut, names(poldb))])
  # names(tmpbrick) <- regmatches(grep(pollut, names(poldb), value=T), 
  #                               regexpr("^\\d{4}", grep(pollut, names(poldb), value = T)))
  # try(print(spplot(tmpbrick)))
  # rm(tmpbrick)
  try(print(spplot(poldb[[grep(pollut, names(poldb))]], main = pollut)))
  dev.off()
}

pdf(paste0("graphs/Bz_annual_brick.pdf"),width = .71*15*2, height=1.5*2)
spplot(log10(poldb[[grep("Bz_annual", names(poldb))]]), 
       main = expression(paste("Benzene (", log[10], " of annual mean concentration in µg.", m^-3, ")", sep = "")))
dev.off()



###### OA-wide means ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### 
# Load OA boundary 
load(paste0(shpfiles, "UK_2001_OA_polygons.RData"))
row.names(oashp) <- oashp@data$oa01cd
# removing water bodies
# plot(oashp[which(is.na(oashp@data$oa01cd)),])
oashp <- oashp[which(!is.na(oashp@data$oa01cd)),]

# Extracting the set of weights to be applied to each grid square for every polygon 
# This saves considerable time rather that running the interpolation for each year and pollutant
# Here we use the fact that the grid structure produced by Ricardo is stable across time and pollutants (OSGB 1km grid)
# This still takes 3 hours
load("data_outputs/grid.structure.RData")
Sys.time()
OA_gridweights <- extract(grid.str, oashp, weights = T, normalizeWeights = T, df = T)
Sys.time()
OA_gridweights <- merge(OA_gridweights, data.frame(list(ID = 1:length(oashp$oa01cd), oa01cd = oashp$oa01cd)), all.x = T)
OA_gridweights$oa01cd <- as.character(OA_gridweights$oa01cd)
OA_gridweights <- OA_gridweights[,c("oa01cd", "ukgridcode", "weight")]
save(OA_gridweights, file = "data_outputs/OA_gridweights.RData")


#and extracting means with data.table which is fastest - a 64bit computer with 32Gb of RAM should manage this)
library(data.table)
load("data_outputs/OA_gridweights.RData")

OA_gridweights <- data.table(OA_gridweights)

# remove missing grid codes
OA_gridweights <- na.omit(OA_gridweights)

OA_mean <- merge(data.table(OA_gridweights), data.table(pollution_db_lg), 
                 all.x = T, all.y = F, by = "ukgridcode",
                 allow.cartesian=T)
# Careful, this is a slow step (~20 minutes to complete) 
# you can make it fast by commenting out " nb_grids =uniqueN(ukgridcode), "
OA_mean <- OA_mean[,list(mean_concentration = sum(value * weight, na.rm=T)/sum(weight * as.numeric(!is.na(value)), na.rm=T),
                         sd_concentration = sd(value, na.rm = T),
                         nb_grids = uniqueN(ukgridcode),
                         nb_missing =  sum(as.numeric(is.na(value))),
                         check = sum(weight* as.numeric(!is.na(value)))),
                    by = list(oa01cd, year, pollutant)]
OA_mean <- OA_mean[!is.na(OA_mean$pollutant),]

save(OA_mean, file = "data_outputs/OA_mean_longfile.RData")

OA_mean_w <- (dcast(OA_mean, oa01cd + year ~ pollutant, value.var = "mean_concentration"))

save(OA_mean_w, file = "data_outputs/OA_mean_simplewidefile.RData")


#QUality control

OAmissing <- data.table(OA_mean)
OAmissing <- OAmissing[, .(mean(is.na(mean_concentration)),sum(is.na(mean_concentration))), by = .(pollutant, year)]
names(OAmissing)[3] <- "pct_missing"
names(OAmissing)[4] <- "nb_OA_missing"
write.csv(OAmissing, file = "quality_control/QC_OA2001_polygon_concentrations.csv", row.names = F)

#OAmissing <- unique(OA_mean[is.na(mean_concentration), oa01cd])


###### CENTROID VALUES ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### ###### 
# Load OA centroids
load(paste0(shpfiles, "centroids/UK_2001_OA_weightedcentroids.RData"))
 
# overlaying on a map to check the transformation was successful
pdf("graphs/centroid_raster.pdf", 15, 15)
print(plot(poldb$PM10_annual[["X2002"]], cex = 0.1))
print(points(oacent, pch=3, cex = .01))
dev.off()

pdf("graphs/centroid_raster_missing.pdf", 15, 15)
print(plot(poldb$PM10_annual[["X2002"]], cex = 0.1))
print(points(oacent[which(oacent$oa01cd %in% OAmissing),], pch=3, cex = .01))
dev.off()

# Extracting pollution information at OA centroids
OAcent_pollution <- list()
for(pollutant in names(poldb)){
OAcent_pollution[[pollutant]]  <- extract(poldb[[pollutant]], oacent, df = T)
OAcent_pollution[[pollutant]]$ID <- oacent@data$oa01cd
names(OAcent_pollution[[pollutant]])[1] <- "oa01cd"
OAcent_pollution[[pollutant]]$pollutant <- pollutant
}
str(OAcent_pollution)
library(dplyr)
OAcent_pollution <- bind_rows(OAcent_pollution)
OAcent_pollution <- reshape(OAcent_pollution, direction = "long",
                idvar = c("oa01cd", "pollutant"),
                varying = sort(grep("^X20[0-9][0-9]", names(OAcent_pollution), value = T)),
                v.names = "centroid_concentration", 
                timevar = "year", 
                times = sort(gsub("^X", "", grep("^X20[0-9][0-9]", names(OAcent_pollution), value = T))))
OAcent_pollution$oa01cd <- as.character(OAcent_pollution$oa01cd)
attr(OAcent_pollution, "reshapeLong") <- NULL
save(OAcent_pollution, file = paste0("data_outputs/OA2001_centroid_concentrations.RData"))



# Quality assurance
OACmissingPM25_2004 <- unique(OAcent_pollution[OAcent_pollution$year == "2002" & 
                                        is.na(OAcent_pollution$centroid_concentration) &
                                        OAcent_pollution$pollutant == "PM25_annual", "oa01cd"])
length(OACmissing)
OACmissing <- data.table(OAcent_pollution)
OACmissing <- OACmissing[, .(mean(is.na(centroid_concentration)),sum(is.na(centroid_concentration))), by = .(pollutant, year)]
names(OACmissing)[3] <- "pct_missing"
names(OACmissing)[4] <- "nb_OA_missing"
write.csv(OACmissing, file = "quality_control/QC_OA2001_centroid_concentrations.csv", row.names = F)


pdf("graphs/centroid_raster_missing.pdf", 15, 15)
print(plot(poldb$PM25_annual[["X2002"]], cex = 0.1))
print(points(oacent[which(oacent$oa01cd %in% OACmissingPM25_2004),], pch=3, cex = .01))
dev.off()



#write.csv(OAcent_pollution, file = paste0("data_outputs/OA2001_centroid_concentrations.csv"), row.names = F)
rm(OAcent_pollution)



###############################
###############################
#### Looking up postcode centroid values

# load the UK grid code structure

# Load ONS Postcode Database for latitude and longitudes (or OS grids!)
# extracting ONLY the first 100 rows
nspd <- read.csv("//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/ONSPD_AUG_2017_UK/Data/ONSPD_AUG_2017_UK.csv", 
                 stringsAsFactors = F, nrows = 100)[,c("pcd", "pcd2", "pcds", "dointr", "doterm", "oslaua", "gor", "nuts", "oa01", "lsoa01", "lat", "long", "oseast1m", "osnrth1m")]

nspd <- read.csv("~/0 Datasets/shp/ONSPD_FEB_2017_UK/Data/ONSPD_FEB_2017_UK.csv", 
                 stringsAsFactors = F, nrows = 100)[,c("pcd", "pcd2", "pcds", "dointr", "doterm", "oslaua", "gor", "nuts", "oa01", "lsoa01", "lat", "long", "oseast1m", "osnrth1m")]


#UK 1km grid coordinates
nspd[,c("x","y")] <- trunc(nspd[,c("oseast1m","osnrth1m")] /1000) *1000 + 500 


save(nspd, file = "#wherever you store this on IDHS")

#Then lookup the pollution using the x, y coordinates.

samplepcd <- data.frame(list(year = 1996, month = 05, pcd = "Ab1 0AA"))
samplepcd$pcd <- toupper(samplepcd$pcd)
samplepcd<-sqldf("select * from samplepcd left join nspd on samplepcd.pcd = nspd.pcd
                 where samplepcd.year <= leftstr(nspd.doterm,4) and samplepcd.month <= rightstr(nspd.doterm,2) 
                 and samplepcd.year >= leftstr(nspd.dointr,4) and samplepcd.month >= rightstr(nspd.dointr,2) ")

#started at 17:37
Sys.time()
samplepcd

head(pollution_db_lg)


plot(gridraster)
points(samplepcd)

samplepcd@data$ukgridcode <- extract(gridraster, samplepcd)

# turning the samplepcd into plain data.frame again and 
# looking up pollution data with a simple left join


# load pollution data
load(file = paste0(ricardo, "pollution_database_long.RData"))


samplepcd <- samplepcd@data
sqldf("select * from samplepcd left join pollution_db_lg ")
str(pollution_db_lg)


