# README #

### Description ###

This syntax takes various point and polygon data as an input and returns tables containing pollution informationextracts information from multiple pollution rasters 

### How does it work? ###

Syntax file `geographical_matching.R` is the main file to run..

* First, edit the initial lines to adapt the year span required, and the destination of pollution files
* Then uncomment and run the first block in order to download and process the pollution data
* Then you can feed 
  * a `SpatialPointsDataFrame` object to function `extract()` to extract raster values
	* a `SpatialPoligonsDataFrame` object to function `extract()` to extract means, minima and maxima raster values for each polygon.

### Who do I talk to? ###

Peter Dutey-Magni

[p.dutey-magni@ucl.ac.uk](mailto:p.dutey-magni@ucl.ac.uk)
