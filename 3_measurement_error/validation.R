
# programmed with  ‘openair’ version 2.1-5

# Packages
library("raster")
library("rgdal")
library("sqldf")
epsg = make_EPSG()
OSGBproj <- epsg$prj4[grep("# OSGB 1936",x = epsg$note)][3]
rm(epsg)
library(jsonlite)
library(openair)
library(dplyr)
load("../raster_extraction/data_outputs/grid.structure.RData")
str(grid.str)

AURN_stations <- importMeta(source = "aurn", all = T)
PCD <- list()
for(i in 1:dim(AURN_stations)[1]){
  try(PCD[[i]] <- flatten(fromJSON(paste("https://api.postcodes.io/postcodes?lon=", AURN_stations$longitude, "&lat=",  AURN_stations$latitude, sep = "")[i])$result[1, ]))
}

PCD <- bind_rows(PCD, .id = "id")
PCD$id <- as.integer(PCD$id)
lk <- data.frame(list(id = c(1:length(AURN_stations$code)),
                      station.code = AURN_stations$code), stringsAsFactors = F)
PCD <- merge(PCD, lk, by = "id", all.x = T)

coordinates(AURN_stations) <- AURN_stations[,c("longitude", "latitude")]
AURN_stations@proj4string <- CRS('+proj=longlat +datum=WGS84')
AURN_stations <- spTransform(AURN_stations, CRS(OSGBproj))
AURN_stations$ukgridcode <- extract(grid.str, AURN_stations, df = T)$ukgridcode
oashp <- shapefile("~/0 Datasets/shp/OA_2001_clipped_gen/Output_Areas_December_2001_Generalised_Clipped_Boundaries_in_England_and_Wales.shp")
row.names(oashp) <- oashp@data$oa01cd
AURN_stations$oa01cd <- extract(oashp, AURN_stations, df = T)$oa01cd
AURN_stations <- merge (AURN_stations, PCD, by.x = "code", by.y = "station.code", all.x = T)

load("~/0 Datasets/shp/GB_2001_OA_polygons.RData")
AURN_stations <- spTransform(AURN_stations, oashp@proj4string)
AURN_stations@data$oa01cd <- over(AURN_stations, oashp)$oa01cd
head(AURN_stations@data)
save(AURN_stations, file = "AURN_stations.RData")
save(PCD, file = "AURN_postcodes.RData")
rm(lk)
write.table(AURN_stations, "c:/Local/AURN_stations.txt", row.names = F, na="",
            quote=F, eol = "\n", sep = "\t")



# download pollution
AURN_poll <- list()
for(site in AURN_stations$code){
  for(years in 1:3){
  AURN_poll[[site]][[years]] <- try(importAURN(site = site, year = list(2001:2006, 2007:2012, 2013:2016)[[years]],
                                        pollutant = "all", 
                                        hc = FALSE, meta = F, verbose = T))
  }
}

AURN_poll <- sapply(AURN_poll, bind_rows)
AURN_poll <- bind_rows(AURN_poll)
names(AURN_poll) <- gsub("\\.", "", names(AURN_poll))
save(AURN_poll, file = "AURN_pollution.RData")



#### Average pollution data across year
load("AURN_stations.RData")
load("AURN_postcodes.RData")
load("AURN_pollution.RData")

library(sqldf)
library(lubridate)
AURN_poll$year <- year(AURN_poll$date)
AURN_poll$o3 <- as.integer(AURN_poll$o3 > 120)

polyear <- sqldf(paste("select code, year, avg(pm10) as AURN_annual_mean, sum(1) as datapoints, 'PM10_annual' as pollutant ",
                       "from AURN_poll where pm10 is not NULL group by site, code, year UNION ",
                       "select code, year, avg(pm25) as AURN_annual_mean, sum(1) as datapoints, 'PM25_annual' as pollutant ",
                       "from AURN_poll where pm25 is not NULL group by site, code, year UNION ",
                       "select code, year, avg(o3) as AURN_annual_mean, sum(1) as datapoints, 'O3_120µgexceed' as pollutant ",
                       "from AURN_poll where o3 is not NULL group by site, code, year"))
table(polyear$pollutant, polyear$year)

save(polyear, file = "AURN_annualmeans.RData")
  

##### Extract modelled pollution values
load("AURN_stations.RData")
load("AURN_postcodes.RData")
load("AURN_annualmeans.RData")

# get the OA references of stations
 
# import OA grid reference weights
load("~/0 Datasets/Ricardo/pollution_database_long.RData")
pollution_db_lg$pollutant <- gsub("Â", "", pollution_db_lg$pollutant)
ricardo_GPS <-  pollution_db_lg[pollution_db_lg$ukgridcode %in% unique(AURN_stations@data$ukgridcode), ]
ricardo_GPS <- ricardo_GPS[, c("ukgridcode", "year", "pollutant", "value")]
names(ricardo_GPS)[4] <- "ricardo_AURN_location"

load("~/0 Datasets/Ricardo/OA2001_centroid_concentrations.RData")
OAcent_pollution <- OAcent_pollution[OAcent_pollution$oa01cd %in% AURN_stations@data$oa01cd[!is.na(AURN_stations@data$oa01cd)],]
# to use if this is still not in long file shape
OAcent_pollution <- reshape(OAcent_pollution, varying = c(16:17, 2:14), sep = "", direction = "long")
OAcent_pollution$oa01cd <- as.character(OAcent_pollution$oa01cd)
names(OAcent_pollution)[3] <- "year"
names(OAcent_pollution)[4] <- "ricardo_OA_pwc"
OAcent_pollution <- subset(OAcent_pollution, select = -c(id) )

load("../raster_extraction/data_outputs/OA_gridweights.RData")
OA_gridweights <- OA_gridweights[OA_gridweights$oa01cd %in% AURN_stations@data$oa01cd[!is.na(AURN_stations@data$oa01cd)],]
pollution_db_lg <- pollution_db_lg[pollution_db_lg$ukgridcode %in% unique(OA_gridweights$ukgridcode),]
AURN_ricardo <- merge(OA_gridweights, pollution_db_lg, by = "ukgridcode", all.x = T)
AURN_ricardo <- sqldf("select oa01cd, year, pollutant, sum(value*weight) as ricardo_OA_mean from AURN_ricardo where oa01cd is not NULL group by oa01cd, year, pollutant ")


AURN_ricardo <- merge(AURN_ricardo, OAcent_pollution, all.x = T, all.y = T)
AURN_ricardo <- merge(AURN_stations@data[, c("code", "oa01cd", "ukgridcode")], AURN_ricardo, by = "oa01cd", all.x = T, all.y = T)
AURN_ricardo <- merge(AURN_ricardo, ricardo_GPS, by = c("ukgridcode", "year", "pollutant"), all.x= T, all.y=T)
AURN_ricardo <- subset(AURN_ricardo, select = -c(ukgridcode, oa01cd))
save(AURN_ricardo, file = "AURN_ricardo.RData")
rm(pollution_db_lg)
rm(OA_gridweights)
rm(OAcent_pollution)
rm(ricardo_GPS)


##################
# VALIDATION 
##################
load("AURN_stations.RData")
load("AURN_postcodes.RData")
load("AURN_annualmeans.RData")
load("AURN_ricardo.RData")
load("AURN_annualmeans.RData")
oaRUC <- read.csv("~/0 Datasets/shp/Rural_Urban_Classification_(2001)_for_OAs_in_England_and_Wales/RUC_OA_2001_EW_LU.csv", 
                  stringsAsFactors = F)[,c("GOR01NM", "OA01CD", "RUC01NM", "Morphology.Name", "Context.Name")]
AURN_stations$site.type[AURN_stations$site.type == "unknown unknown" & AURN_stations$code == "BEL3"] <- "Urban Traffic"
AURN_stations$site.type[AURN_stations$site.type == "unknown unknown"] <- "Urban Background"
AURN_stations$altitude <- as.numeric(AURN_stations)

write.csv(AURN_stations, "stationtomap.csv", row.names = F)


valid <- merge(AURN_ricardo[, c("code", "year", "pollutant", "ricardo_OA_mean", "ricardo_OA_pwc", "ricardo_AURN_location")],
               polyear, by = c("code", "year", "pollutant"), all.x = T, all.y = T)
valid <- valid[!is.na(valid$code) & !is.na(valid$year),]
valid <- merge(AURN_stations@data, valid,  by = "code", all.x=T, all.y=T)
valid <- merge(valid, oaRUC, by.x = "oa01cd", by.y = "OA01CD", all.x = T, all.y = F)
valid$GOR01NM[is.na(valid$GOR01NM)] <- "Scotland/NI"
valid$GOR01NM[ valid$GOR01NM==""] <- "Wales"
write.csv(valid, file = "validata.csv", row.names = F)

valid <- read.csv("validata.csv", stringsAsFactors = F)
valid <- valid[valid$pollutant %in% c("PM25_annual","PM10_annual"),] #  "O3_120µgexceed" O3 is f*cked

# valid <- split(valid, valid$pollutant)

library(lattice)
library(latticeExtra)
library(plyr)


# Validation of AURN annual means against Ricaardo value
tata <- ddply(valid, .variables = c("pollutant", "year", "site.type"), .fun = function(X) {
  dudu <- na.omit(X[,c("ricardo_AURN_location","AURN_annual_mean")])
  diff = dudu[,1]- dudu[,2]
  monitorvar =  var(dudu[,2])
  ricardovar = var(dudu[,1])
  n = dim(dudu)[1]
   return(data.frame(list( r2 = cor(dudu[,1], dudu[,2])^2, meandiff = mean(diff), SSQdiff = sum(diff^2),
                           monitorvar = monitorvar, ricardovar = ricardovar, ratio = ricardovar/monitorvar, n =  n), stringsAsFactors = F))
})
write.csv(na.omit(tata), file = "results/ricardo_aurn.csv", row.names = F)


 

library(ggplot2)
 
p10 <- ggplot(valid, aes(x =  factor(year), y = ((ricardo_AURN_location/AURN_annual_mean)*100 -100))) + 
       geom_boxplot(outlier.size = .1) +  coord_flip() +  
  scale_y_continuous(name = "Relative bias (%) modelled pollution (1km grid)") +geom_jitter( size = .1 ,position=position_jitter(w=0.1,h=0.1))+
  facet_wrap(~ pollutant) + theme_bw() +
  theme(panel.grid.major = element_line(colour = "#d3d3d3"),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank(),
        plot.title = element_text(size = 14, family = "Tahoma", face = "bold"),
        text=element_text(family = "Tahoma"),
        axis.title = element_text(face="bold"),
        axis.text.x = element_text(colour="black", size = 11),
        axis.text.y = element_text(colour="black", size = 9),
        axis.line = element_line(size=0.5, colour = "black")) +geom_hline(yintercept = 0, colour = "red")
print(p10)

 
p10 <- ggplot(valid, aes(x =  factor(GOR01NM), y = ((ricardo_OA_mean/ricardo_AURN_location )*100 -100))) + 
  geom_boxplot(outlier.size = .1) +  coord_flip() +  
  scale_y_continuous(name = "Relative difference (%) between estimate and output area mean (1km grid)") +geom_jitter( size = .1 ,position=position_jitter(w=0.1,h=0.1))+
  facet_wrap(~ pollutant) + theme_bw() +
  theme(panel.grid.major = element_line(colour = "#d3d3d3"),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank(),
        plot.title = element_text(size = 14, family = "Tahoma", face = "bold"),
        text=element_text(family = "Tahoma"),
        axis.title = element_text(face="bold"),
        axis.text.x = element_text(colour="black", size = 11),
        axis.text.y = element_text(colour="black", size = 9),
        axis.line = element_line(size=0.5, colour = "black")) +geom_hline(yintercept = 0, colour = "red")
print(p10)


library(lme4)
summary(lmer(ricardo_AURN_location-AURN_annual_mean~   (1|code), data = valid[valid$pollutant == "PM10_annual",]))

View(valid[valid$pollutant == "PM25_annual" & valid$year ==2007 & !is.na(valid$ric_to_AURN), ])
 
table(valid$zone_name[is.na(valid$GOR01CD)], useNA = "a")
table(valid[which(valid$GOR01NM == ""),"site"])

  # geom_boxplot(fill = fill, colour = line,
  #              alpha = 0.7) +
  # scale_y_continuous(name = "Mean ozone in\nparts per billion",
  #                    breaks = seq(0, 175, 50),
  #                    limits=c(0, 175)) +
  # scale_x_discrete(name = "Month") +
  # ggtitle("Boxplot of mean ozone by month") +
  # theme_bw() +
  # theme(plot.title = element_text(size = 14, family = "Tahoma", face = "bold"),
  #       text = element_text(size = 12, family = "Tahoma"),
  #       axis.title = element_text(face="bold"),
  #       axis.text.x=element_text(size = 11)) +
  # facet_grid(. ~ pollutant)




# Validation of AURN annual means against Ricardo OA mean
tata <- ddply(valid, .variables = c("pollutant", "RUC01CD", "RUC01NM"), .fun = function(X) {
  dudu <- na.omit(X[,c("ricardo_OA_mean","AURN_annual_mean")])
  diff = dudu[,1]- dudu[,2]
  monitorvar =  var(dudu[,2])
  ricardovar = var(dudu[,1])
  n = dim(dudu)[1]
  return(data.frame(list( r2 = cor(dudu[,1], dudu[,2])^2, meandiff = mean(diff), SSQdiff = sum(diff^2), monitorvar = monitorvar,
                          ricardovar = ricardovar, ratio_ric_to_monit = ricardovar/monitorvar, n =  n), stringsAsFactors = F))
})
write.csv(na.omit(tata), file = "results/OAricardo_aurn.csv", row.names = F)

mod <- try(summary(lm(ricardo_AURN_location~AURN_annual_mean +altitude , na.action = na.omit, data = X))) 


xyplot(r2~year|site.type, data = tata)
 

xyplot(ricardo_AURN_location~AURN_annual_mean |  year, data = valid$PM10_annual)

xyplot(ricardo_AURN_location~ricardo_OA_mean | site.type, data = valid$PM25_annual)


table(valid[valid$site.type == "unknown unknown", c("code", "site")])


########### simu

n=20
X1=rnorm(n)
X2 = X1 + rnorm(n)
X3 = X2 + rnorm(n)
X <- data.frame(list(X=c(X1, X2, X3), time=rep(1:n, 3)), occ=c(rep(1,n), rep(2, n), rep(3,n)))


plot(X$X, X$occ, pch=3, cex=1.3)
abline(1,0, lty=2, col="grey50")
abline(2,0, lty=2, col="grey50") 
abline(3,0, lty=2, col="grey50")
for(i in 1:n) 
{ 
  lines(X[X$time==i,"X"], 1:3,type="l",lty=1, col="grey50")
} 
points(X$X, X$occ, pch=3, cex=1.3)



 