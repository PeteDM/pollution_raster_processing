**********************************************
* Millenium Cohort Study
* Processing dates of residential moves
* Peter Dutey-Magni
* 24/10/2017
**********************************************

* this programme processes subsequent waves of MCS. It takes the output area of interview,
* the date of interview, and the date of the last move as inputs. 
* It then returns a table with the assumed OA of residence for each MCSID
* for every month since the start of the study.

* Define the directory where UK Data Service files are located (MCS1, MCS2, etc.) and end with a single forward slash "/"
global mcsarchives `"//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/"'

* Define directory where to save results
global output `"C:/Local/"'
 
use  `"${mcsarchives}MCS1/stata11/mcs1_geographically_linked_data.dta"', clear
rename *, lower
keep mcsid aactry00
merge 1:1 mcsid using `"${mcsarchives}MCS2/stata11_se/mcs2_geographically_linked_data.dta"'
rename *, lower
keep mcsid aactry00 bactry00
merge 1:1 mcsid using `"${mcsarchives}MCS3/stata11_se/mcs3_geographically_linked_data.dta"'
rename *, lower
keep mcsid aactry00 bactry00 cactry00
merge 1:1 mcsid using `"${mcsarchives}MCS4/stata11_se/mcs4_geographically_linked_data.dta"'
rename *, lower
keep mcsid aactry00 bactry00 cactry00 dactry00
rename *, upper
merge 1:1 MCSID using `"${mcsarchives}MCS5/stata11/mcs5_geographically_linked_data.dta"'
rename *, lower
keep mcsid aactry00 bactry00 cactry00 dactry00 eactry00
rename *, upper
*merge ** MCS 6 missing!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
rename *, lower
keep mcsid aactry00 bactry00 cactry00 dactry00 eactry00 fhcntry0

save `"${output}OA_history.dta"', replace

*******************************************
*******************************************
clear all
set maxvar 10000

use `"${mcsarchives}MCS1/stata11/mcs1_parent_interview.dta"', clear
keep mcsid ahintm00 ahinty00 ammoad00 ammomo00  ahcdbma0 ahcdbya0
gen dob = mdy(ahcdbma0, 15, ahcdbya0)
format %td  dob
drop ahcdbma0 ahcdbya0  

merge 1:1 mcsid using  `"${mcsarchives}MCS2/stata11_se/mcs2_parent_interview.dta"'
replace dob = mdy(bhcdbma0, 15, bhcdbya0)  if dob == .
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00

merge 1:1 mcsid using  `"${mcsarchives}MCS3/stata11_se/mcs3_parent_interview.dta"'
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00 chidtm00 chidty00 chadsa00 cmmoad00 cmmomo00   
merge 1:1 mcsid using  `"${mcsarchives}MCS4/stata11_se/mcs4_parent_interview.dta"'
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00 chidtm00 chidty00 chadsa00 cmmoad00 cmmomo00 dhintm00 dhinty00 dhadsa00 dmmoad00 dmmomo00
save `"${output}extract_move_dates.dta"', replace

use `"${mcsarchives}MCS5/stata11/mcs5_parent_interview.dta"', clear
rename *, lower
keep mcsid  eelig00 epintm00 epinty00 ehadsa00 epmoad00 epmomo00 
duplicates tag mcsid, gen(duplic_mcsid)
keep if duplic_mcsid == 0 | (duplic_mcsid == 1 & eelig00 == 1)
drop eelig00 duplic_mcsid

merge 1:1 mcsid using `"${output}extract_move_dates.dta"'
drop _merge
save `"${output}extract_move_dates.dta"', replace

use `"${mcsarchives}MCS6/stata11/mcs6_parent_interview.dta"', clear
rename *, lower
keep mcsid   felig00  fpinty00 fpintm00 fhadsa00 fpmoad00 fpmomo00 
* ignoring other variables fhmoyry0 fhmoyrm0 which are new but not defined
duplicates tag mcsid, gen(duplic_mcsid)
* if there are several lines of information for a single family, keeping the time of move for the main informant
keep if   duplic_mcsid == 0 | (duplic_mcsid == 1 & felig00 == 1)
drop felig00 duplic_mcsid

merge 1:1 mcsid using `"${output}extract_move_dates.dta"'
drop _merge
save `"${output}extract_move_dates.dta"', replace

use `"${output}extract_move_dates.dta"', clear
gen aint = mdy(ahintm00, 15, ahinty00)
gen bint = mdy(bhidtm00, 15, bhidty00)
gen cint = mdy(chidtm00, 15, chidty00)
gen dint = mdy(dhintm00, 15, dhinty00)
gen eint = mdy(epintm00, 15, epinty00)
gen fint = mdy(fpintm00, 15, fpinty00)

gen amov = mdy(ammomo00, 15, ammoad00)
gen bmov = mdy(bmmomo00, 15, bmmoad00)
gen cmov = mdy(cmmomo00, 15, cmmoad00)
gen dmov = mdy(dmmomo00, 15, dmmoad00)
gen emov = mdy(epmomo00, 15, epmoad00)
gen fmov = mdy(fpmomo00, 15, fpmoad00)

format %td aint bint cint dint eint fint amov bmov cmov dmov emov fmov

keep mcsid bmadsa00 chadsa00 dhadsa00 ehadsa00 fhadsa00 dob aint bint cint dint eint fint amov bmov cmov dmov emov fmov
order mcsid bmadsa00 chadsa00 dhadsa00 ehadsa00 fhadsa00 dob aint bint cint dint eint fint amov bmov cmov dmov emov fmov
save `"${output}extract_move_dates.dta"', replace

saveold "\\filestore.soton.ac.uk\users\pfdm1m17\mydocuments\0 Datasets\MCS\extract_move_dates_old2.dta", replace version(11)
* then must switch to R!

