# README #

### Description ###

This syntax processes MCS files to collate the address history of every family (listed by `mcsid`) and the length of time spent at each location.

The syntax is set up to extract 2001 census Output Areas but can be adapted to extract other geographical information.

It consists of 

* one Stata file `mcsaddresshistory.do` which merges all six waves of the MCS; extracts information about places of interviews; and information about dates of move
* one R file `mcsaddresshistory2.R` to run in a second step; it imports dates of residential moves and processes them to return a long file containing, for each family `mcsid`, the number of years spent at each of the addresses where the interview took place on waves `a`, `b`, `c`, `d`, `e`, and `f`.

Files produced by both these programmes can be used to then match Output Areas, pollution exposure, and duration of exposure.

### Who do I talk to? ###

Peter Dutey-Magni

[p.dutey-magni@ucl.ac.uk](mailto:p.dutey-magni@ucl.ac.uk)
