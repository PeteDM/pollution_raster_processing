/** 
 *  @project MCS Effect of Air Pollution of Cognitive Development
 *  @file    0_main_syntax.do
 *  @author  Peter Dutey-Magni (PDM)
 *  @email   p.dutey-magni@ucl.ac.uk
 *  @date    13/06/2018
 *  @version 2.0 
 *  
 *  @section DESCRIPTION
 *  This syntax makes calls to every other syntax chunk to execute the entire
 *  analysis.
 *
 *  @section VERSION HISTORY
 *  Date            Author      Version    Revision
 *  25/05/2018      PDM         1.0        Initial commit
 *  13/06/2018      PDM         2.0        Added line for analyses
 */

clear all
macro drop _all
set maxvar 10000
 
* Sets the initial paths ***************************************************
* Run this every time
run 01_set_filepaths_cleaning.do

* Loads datasets, selects variables, saves into working directory **********

* Note: this does not need to be executed every time 
do 02_select_variables.do
do 03_generate_address_history.do
shell R "03_process_address_history.R"
do 04_build_extracts.do
clear all

* Begin analysis!!!
use "$pathOutput\main_extract.dta", clear
* here we should add new syntax files for separate pieces of analysis with a description
* of what it does. I suggest this is executed silently and saves results in a log

log using $pathLogs/log_analysis1_`: di  %tdCY-N-D  daily("$S_DATE", "DMY")' 
do 05_analysis1.do
log close




* end of script

