/** 
 *  @project MCS Effect of Air Pollution of Cognitive Development
 *  @file    04_build_extracts.do
 *  @author  Peter Dutey-Magni (PDM)
 *  @email   p.dutey-magni@ucl.ac.uk
 *  @date    13/05/2018
 *  @version 2.0 
 *  
 *  @section DESCRIPTION
 *  This combines all files saved under the output directory into extracts
 *  with the appropriate structure for regression analysis.
 *
 *  @section VERSION HISTORY
 *  Date            Author      Version    Revision
 *  25/05/2018      PDM         1.0        Initial commit
 *  13/06/2018      PDM         2.0        Imports all files except parent interview DVs
 */

 
 /* to do list:
  IMPORT "$pathOutput\mcs5_parent_derived.dta" - this requires deciding which parents/respondents
  should be considered when taking parental socio-economic characteristics
  
  
*/
 
 
 
* Starting to build a spine of CMs

use "$pathOutput\mcs1_hhgrid.dta", clear
keep  mcsid ahintm00 ahinty00 cnum ahcsex00 ahcdbm00 ahcdby00 ahcage00 ahcprs00
* removing persons who are not cohort members
drop if cnum == -1

* add individual level Wave 1 data
merge 1:1 mcsid cnum using "$pathOutput\mcs1_parent_interview.dta"
drop _merge
merge 1:1 mcsid cnum using "$pathOutput\mcs1_derived_variables_CMlevel.dta"
drop if _merge==2
drop _merge
* add family level Wave 1 data
merge m:1 mcsid using "$pathOutput\mcs1_derived_variables_familylevel.dta"
drop _merge

* add individual level Wave 2 data
merge 1:1 mcsid cnum using "$pathOutput\mcs2_parent_interview_CMlevel.dta"
drop _merge
* add family level Wave 2 data
merge m:1 mcsid using "$pathOutput\mcs2_derived_variables.dta"
drop _merge


* add individual level Wave 3 data
merge 1:1 mcsid cnum using "$pathOutput\mcs3_parent_interview_CMlevel.dta"
drop _merge
* add family level Wave 3 data
merge m:1 mcsid using "$pathOutput\mcs3_derived_variables.dta"
drop _merge

* add individual level Wave 4 data
merge 1:1 mcsid cnum using "$pathOutput\mcs4_parent_interview_CMlevel.dta"
drop _merge
* add family level Wave 4 data
merge m:1 mcsid using "$pathOutput\mcs4_derived_variables.dta"
drop _merge

* add individual level Wave 5 data
merge 1:1 mcsid cnum using "$pathOutput\mcs5_parent_cm_interview.dta"
drop _merge
* add family level Wave 5 data
merge m:1 mcsid using "$pathOutput\mcs5_family_derived.dta"
drop _merge

* not importing "$pathOutput\mcs5_parent_derived.dta"
* unclear which parents to take 


* add individual level Wave 6 data
merge 1:1 mcsid cnum using "$pathOutput\mcs6_cm_derived.dta"
drop _merge
merge 1:1 mcsid cnum using "$pathOutput\mcs6_parent_cm_interview.dta"
drop _merge

* adding assessment data
merge 1:1 mcsid cnum using  "$pathOutput\mcs2_child_assessment_data.dta"
drop _merge
merge 1:1 mcsid cnum using  "$pathOutput\mcs3_child_assessment_data.dta"
drop _merge
merge 1:1 mcsid cnum using  "$pathOutput\mcs4_cm_assessment.dta"
drop _merge
merge 1:1 mcsid cnum using  "$pathOutput\mcs5_cm_assessment.dta"
drop _merge

* and finally, longitudinal information
merge m:1 mcsid using "$pathOutput\mcs_longitudinal_family_file.dta"
drop _merge

save "$pathOutput\main_extract.dta", replace

clear all
* end of script
