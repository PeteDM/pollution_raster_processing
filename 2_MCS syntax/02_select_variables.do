/** 
 *  @project MCS Effect of Air Pollution of Cognitive Development
 *  @file    02_select_variables.do
 *  @author  Peter Dutey-Magni (PDM)
 *  @email   p.dutey-magni@ucl.ac.uk
 *  @date    13/06/2018
 *  @version 2.0 
 *  
 *  @section DESCRIPTION
 *  This syntax: 
 *     - loads each primary datasets
 *     - changes variables names to lowercase
 *     - removes variables that are not needed 
 *     - saves each in the working directory.
 *
 *  @section VERSION HISTORY
 *  Date            Author      Version    Revision
 *  25/05/2018      PDM         1.0        Initial commit
 *  13/06/2018      PDM         2.0        Deduplicated some of the files to produce CM-level extracts
 */


* Longitudinal file
**********************************************************************************************************************

use "$pathMCSlong\mcs_longitudinal_family_file.dta", clear
rename *, lower
save "$pathOutput\mcs_longitudinal_family_file.dta", replace


* Sweep 1 
**********************************************************************************************************************

use "$pathMCS1\mcs1_parent_interview.dta", clear
rename *, lower
keep mcsid acountry ahintm00 ahinty00 ahcnuma0 ahcnumb0 ahcnumc0 ///
     ambfeva0 ambfevb0 ambfevc0 amsmus0a amsmus0b amsmus0c amsmma00 ampiof00 amsmty00 ///
     amsmev00 amcipr00 amsmch00 amwhch00 amcich00 amsmkr00 apsmus0a apsmus0b apsmus0c apsmus0d ///
	 apsmma00 appiof00 apsmty00 apsmev00 apcipr00 apsmch00 apwhch00 apcich00  amaldr00 amdrof00 ///
	 apaldr00 apdrof00 amunwk00 amunda00 ampuwk00 ampuda00 apunwk00 apunda00 amlpev00 aplpev00
rename ahcnuma0 cnum1
rename ahcnumb0 cnum2
rename ahcnumc0 cnum3
rename ambfeva0 ambfev1
rename ambfevb0 ambfev2
rename ambfevc0 ambfev3
reshape long cnum ambfev, i(mcsid) j(caca) 
drop caca
drop if cnum == .
label variable cnum "S1 HHQ Cohort Member Number"
label variable ambfev "S1 MAIN Ever tried to breastfeed"
save "$pathOutput\mcs1_parent_interview.dta", replace

use "$pathMCS1\mcs1_derived_variables.dta", clear
rename *, lower
keep mcsid adregn00 adctry00 adbwgta0 adbwgtb0 adbwgtc0  adc06ea0 adc11ea0 adc08ea0 adc06eb0 ///
     adc11eb0 adc08eb0 adc06ec0 adc11ec0 adc08ec0 amdagb00 apdagb00 amdgab00 apdgab00 amdnvq00 ///
	 apdnvq00 adgesta0 adgestb0 adgestc0 adhlan00 adnoba00 adnsib00 adoths00 adtots00
save "$pathOutput\mcs1_derived_variables.dta", replace

use "$pathMCS1\mcs1_hhgrid.dta", clear
rename *, lower
label variable ahintm00 "S1 Interview Date (month)"
label variable ahinty00 "S1 Interview Date (year)"
label variable ahcage00 "S1 CM Age at interview (days)"
rename ahcnum00 cnum
save "$pathOutput\mcs1_hhgrid.dta", replace

* Split family and CM records in family-level datasets 
use "$pathOutput\mcs1_derived_variables.dta", clear
keep mcsid adc06ea0 adc11ea0 adc08ea0 adbwgta0 adgesta0 adc06eb0 adc11eb0 adc08eb0 ///
     adbwgtb0 adgestb0 adc06ec0 adc11ec0 adc08ec0 adbwgtc0 adgestc0
rename adc06ea0 ethnic6grp1
rename adc06eb0 ethnic6grp2
rename adc06ec0 ethnic6grp3
rename adc08ea0 ethnic8grp1
rename adc08eb0 ethnic8grp2
rename adc08ec0 ethnic8grp3
rename adc11ea0 ethnic11grp1
rename adc11eb0 ethnic11grp2
rename adc11ec0 ethnic11grp3
rename adbwgta0 adbwgt1
rename adbwgtb0 adbwgt2
rename adbwgtc0 adbwgt3
rename adgesta0 adgest1
rename adgestb0 adgest2
rename adgestc0 adgest3

reshape long ethnic6grp ethnic8grp ethnic11grp adbwgt adgest, i(mcsid) j(cnum)
label variable cnum "Cohort Member Number"
label variable ethnic6grp "S1 DV Cohort Member Ethnic Group - 6 category classification"
label variable ethnic8grp "S1 DV Cohort Member Ethnic Group - 8 category classification"
label variable ethnic11grp "S1 DV Cohort Member Ethnic Group - 11 category classification"
label variable adbwgt "S1 DV Cohort Member birth weight in kilos"
label variable adgest "S1 DV Cohort Member Gestation time in days (estimated)"
save "$pathOutput\mcs1_derived_variables_CMlevel.dta", replace

use "$pathOutput\mcs1_derived_variables.dta", clear
drop adc06ea0 adc11ea0 adc08ea0 adbwgta0 adgesta0 adc06eb0 adc11eb0 adc08eb0 ///
     adbwgtb0 adgestb0 adc06ec0 adc11ec0 adc08ec0 adbwgtc0 adgestc0
save "$pathOutput\mcs1_derived_variables_familylevel.dta", replace



* Sweep 2 
**********************************************************************************************************************	 

use "$pathMCS2\mcs2_parent_interview.dta", clear
rename *, lower
keep mcsid bhidtm00 bhidty00 bhctry00 bhcnuma0 bhcnumb0 bhcnumc0 ///
     bmbfmta0 bmbfeaa0 bmbfmtb0 bmbfeab0 bmbfmtc0 bmbfeac0  bmclsia0 ///
     bmclscaa bmclscab bmclscac bmclscad bmwalcaa bmwalcab bmwalcac bmclsib0 ///
     bmclscba bmclscbb bmclscbc bmclscbd bmclsic0 bmclscca bmclsccb bmclsccc ///
     bmclsccd bmloil00 bmlowc0a bmlowc0b bmlowc0c bmlowc0d bmlowc0e bploil00 ///
     bplowc0a bplowc0b bplowc0c bplowc0d bplowc0e bplowc0f bxloil00 bxlowc0a 

rename bhcnuma0 cnuma
rename bhcnumb0 cnumb
rename bhcnumc0 cnumc

* changing the age last breast feed to months for CM 2 and 3 (already in months for CM 1. Thanks MCS for making my life easy o_O) 
replace bmbfmtb0 = bmbfmtb0 * 12 if bmbfmtb0 >= 0
replace bmbfmtc0 = bmbfmtc0 * 12 if bmbfmtc0 >= 0

reshape long cnum@ bmbfmt@0 bmbfea@0 bmclsi@0 bmclsc@a bmclsc@b bmclsc@c bmclsc@d bmwalc@a bmwalc@b bmwalc@c, i(mcsid) j(caca, string)
drop caca
drop if cnum == .

label values bmbfea0 BMBFEAA0
label variable cnum "S2 HHQ Cohort Member Number"
label variable bmbfmt0 "S2 MAIN Age child last had breast milk (months)"
label variable bmbfea0 "S2 MAIN Age child last had breast milk (text)"
label variable bmclsi0 "S2 MAIN Child any longstanding health conditions"
label variable bmclsca "S2 MAIN What is longstanding health condition(coded) MC1"
label variable bmclscb "S2 MAIN What is longstanding health condition(coded) MC2"
label variable bmclscc "S2 MAIN What is longstanding health condition(coded) MC3"
label variable bmclscd "S2 MAIN What is longstanding health condition(coded) MC4"
label variable bmwalca "S2 MAIN What is longstanding health condition(coded) MC1"
label variable bmwalcb "S2 MAIN What is longstanding health condition(coded) MC3"
label variable bmwalcc "S2 MAIN What is longstanding health condition(coded) MC3"

save "$pathOutput\mcs2_parent_interview_CMlevel.dta", replace

use "$pathMCS2\mcs2_derived_variables.dta", clear
rename *, lower
keep mcsid bdnoba00 bdoths00 bdnocm00 bdtots00 bdnumh00 bdtotp00 bmdnvq00 bpdnvq00
save "$pathOutput\mcs2_derived_variables.dta", replace

use "$pathMCS2\mcs2_child_assessment_data.dta", clear
rename *, lower
keep mcsid bhcnum00 bbasage bbrknage bccass00 bcmeas00 bdbasr00 bdbasa00 bdbasp00 ///
     bdbast00 bcdurm00 bcener00 bcapre00 bbrknage bdbsrc00 bdsrcm00 bdsrcs00 ///
	 bdsrcp00 bdsrcn00
rename bhcnum00 cnum
save "$pathOutput\mcs2_child_assessment_data.dta", replace


* Sweep 3
**********************************************************************************************************************	 

use "$pathMCS3\mcs3_child_assessment_data.dta", clear
rename *, lower
keep mcsid chcnum00 ccindm01 ccindy01 ccwarn00 cccsco00 ccnsco00 ccpsco00 ccpsco00 cdpsabil cdpstscr cccsco00 cdpcabil cdpctscr ccnsco00 cdnvabil cdnvtscr
rename chcnum00 cnum
save "$pathOutput\mcs3_child_assessment_data.dta", replace

use "$pathMCS3\mcs3_derived_variables.dta", clear
rename *, lower
keep mcsid cdnoba00 cdoths00 cdnocm00 cdtots00 cdnsib00 cdcnty00 cdregn00 cdwgt100 cdwgt200 ///
     cdovw100 cdovw200 cmdnvq00 cpdnvq00
save "$pathOutput\mcs3_derived_variables.dta", replace

use "$pathMCS3\mcs3_parent_interview.dta", clear
rename *, lower
* missing: cmclsxbf  cmclsxca cmclsxcb cmclsxcc cmclsxcd cmclsxce cmclsxcf
keep mcsid chidtm00 chidty00 cacnty00 chcnuma0 chcnumb0 chcnumc0 cmclsia0 cmclsxaa cmclsxab ///
     cmclsxac cmclsxad cmclsxae cmclsxaf cmclsib0 cmclsxba ///
	 cmclsxbb cmclsxbc cmclsxbd cmclsxbe cmclsic0 cmloil00 cmlolm00  ///
	 cxpxll00 cxpxlt00 cploil00 cplolm00  
	 
rename chcnuma0 cnuma
rename chcnumb0 cnumb
rename chcnumc0 cnumc

reshape long cnum@ cmclsi@0 cmclsx@a cmclsx@b cmclsx@c cmclsx@d cmclsx@e cmclsx@f, i(mcsid) j(caca, string)
drop caca
drop if cnum == .
drop if cnum == -1

label variable cnum "S3 HHQ Cohort Member Number"
label variable cmclsi0 "S3 MAIN: Whether CM has longstanding illness"
label variable cmclsxa "S3 MAIN: Longstanding illness MC1"
label variable cmclsxb "S3 MAIN: Longstanding illness MC2"
label variable cmclsxc "S3 MAIN: Longstanding illness R1 MC1"
label variable cmclsxd "S3 MAIN: Longstanding illness R1 MC2"
label variable cmclsxe "S3 MAIN: Longstanding illness R2 MC1"
label variable cmclsxf "S3 MAIN: Longstanding illness R2 MC2"

save "$pathOutput\mcs3_parent_interview_CMlevel.dta", replace


* Sweep 4
**********************************************************************************************************************	 

use "$pathMCS4\mcs4_cm_assessment.dta", clear
rename *, lower
keep mcsid dccnum00 dcagey00 dcagem00 dcwrab00 dcwrsd00 dcwrsc00 dcpcab00 dcpcts00 ///
     dctots00 mtotscor maths7sc maths7sa
rename dccnum00 cnum
save "$pathOutput\mcs4_cm_assessment.dta", replace

use "$pathMCS4/mcs4_derived_variables.dta", clear
rename *, lower
keep mcsid  dmpnum00 dppnum00 dmdnvq00 dpdnvq00 ddnoba00 ddoths00 ddnocm00 ddtots00
save "$pathOutput\mcs4_derived_variables.dta", replace

use "$pathMCS4/mcs4_parent_interview.dta", clear
rename *, lower
keep mcsid dhintm00 dhinty00 dhctry00 dhcnuma0 dhcnumb0 dhcnumc0 ///
     dmpnum00 dppnum00 dmtwina0 dmtwinb0 dmtwinc0 dmclsia0 dmclsmab ///
     dmclsmac dmclsmad dmclsla0 dmclsib0 dmclsmbb dmclsmbc dmclsmbd dmclslb0 dmclsic0 ///
     dmclsmcb dmclsmcc dmclsmcd dmclslc0 dmloil00 dmlolm00 dploil00 dplolm00 dxpxll00 ///
     dxpxlt00
	 
rename dhcnuma0 cnuma
rename dhcnumb0 cnumb
rename dhcnumc0 cnumc

reshape long cnum@ dmtwin@0 dmclsi@0 dmclsm@b dmclsm@c dmclsm@d dmclsl@0, i(mcsid) j(caca, string)
drop caca
drop if cnum == .
drop if cnum == -1

label variable cnum "S4 HHQ Cohort Member Number"
label variable dmtwin0 "S4 MAIN Questions about twins"
label variable dmclsi0 "S4 MAIN Whether CM has longstanding illness"
label variable dmclsmb "S4 MAIN Other longstanding illness R1"
label variable dmclsmc "S4 MAIN Other longstanding illness R2"
label variable dmclsmd "S4 MAIN Other longstanding illness R3"
label variable dmclsl0 "S4 MAIN Do CM illnesses limit activity"
 
save "$pathOutput\mcs4_parent_interview_CMlevel.dta", replace

* Sweep 5
**********************************************************************************************************************	 

use "$pathMCS5/mcs5_cm_assessment.dta", clear
rename *, lower
keep mcsid ecnum00 ecintm00 ecinty00 evsraw evsabil evstsco evsaflag  swmttime ///
     swmbe4bx swmbe8bx swmderrs swmderr4 swmstrat swmmtofr swmmtspt swmmttlr ///
	 swmte4bx swmte8bx swmwerrs swmwe4bx swmwe8bx ctest cgtttime cgtdelay ///
	 cgtdtime cgtopbet cgtqofdm cgtriska cgtriskt
rename ecnum00 cnum
save "$pathOutput\mcs5_cm_assessment.dta", replace

use "$pathMCS5/mcs5_parent_cm_interview.dta", clear
rename *, lower
duplicates tag mcsid ecnum00, gen(duplitag)
drop if eresp00 == 2 & duplitag == 1/*remove all partner interviews except where there is only one interview*/
keep mcsid eresp00 epnum00 ecnum00 ecintm00 ecinty00 epclsi00 epcghe00 epclsl00 epclsp00 ///
     epeyepa0 epeyepb0 epeyepc0 epregb00 epherpa0 epherpb0 ephernb0 epheryb0 ephrspb0 ///
	 epherpc0 epwhly00 epwhan00 epwhsd00 epwhsl00 epasma00 epwhex00 epwhcl00 epoevs00 ///
	 epoewm00 epoecg00 epoecq00
rename ecnum00 cnum
save "$pathOutput\mcs5_parent_cm_interview.dta", replace

/* Not importing this yet because need to define whose social class we take
use "$pathMCS5/mcs5_parent_derived.dta", clear
rename *, lower
* removing proxy respondents?
drop if eelig00 ==3
keep mcsid epnum00 eddnvq00 edacaq00
save "$pathOutput\mcs5_parent_derived.dta", replace
*/

use "$pathMCS5/mcs5_family_derived.dta", clear
rename *, lower
keep mcsid eactry00 earegn00 edoths00 ednocm00 edtots00 ednsib00 edhsib00 edssib00 edasib00  
save "$pathOutput\mcs5_family_derived.dta", replace

* Sweep 6
**********************************************************************************************************************	 

use "$pathMCS6/mcs6_parent_cm_interview.dta", clear
rename *, lower
duplicates tag mcsid fcnum00, gen(duplitag)
drop if fresp00 == 2 & duplitag == 1 /*remove all partner interviews except where there is only one interview*/
keep mcsid fpnum00 fcnum00 fpclsi00 fpclsm0a fpclsm0b fpclsm0c fpclsm0d fpclsm0e ///
     fpclsm0f fpclsm0g fpclsm0h fpclsm0i fpclsm0j fpclsm0k fpclsm0l fpclsm0m fpclsm0n ///
	 fpclsm0o fpclsm0p fpclsm0q fpclsm0r fpclsm0s fpclsm0t fpclsm0u fpclsm0v fpclsm0w ///
	 fpclsm0x fpclsm0y fpclsm0z fpclsm1a fpclsm1b fpclsm1c fpclsm1d fpclsl00 fpclsp00 ///
	 fpcltr00 fppcrp00 fpregb00 fplnpr0a fplnpr0b fplnpr0c fplnpr0d fplneva0 fplnevb0 ///
	 fplnevc0 fpasma00 
rename fcnum00 cnum
save "$pathOutput\mcs6_parent_cm_interview.dta", replace

use "$pathMCS6/mcs6_cm_derived.dta", clear
rename *, lower
keep mcsid fcnum00 fcintm00 fcinty00 fdce0600 fdce0800 fdce1100 fcgtoutcm fcgtttime ///
     fcgtdelay fcgtdtime fcgtopbet fcgtqofdm fcgtriska fcgtriskt fcwrdsc
rename fcnum00 cnum
save "$pathOutput\mcs6_cm_derived.dta", replace

use "$pathMCS6/mcs6_parent_derived.dta", clear
rename *, lower
keep mcsid fpnum00 fdnvq00 fpwrdscm fpwrdscp
save "$pathOutput\mcs6_parent_derived.dta", replace

s
* end of script
