/** 
 *  @project MCS Effect of Air Pollution of Cognitive Development
 *  @file    01_set_filepaths_clearning.do
 *  @author  Peter Dutey-Magni (PDM)
 *  @email   p.dutey-magni@ucl.ac.uk
 *  @date    13/06/2018
 *  @version 2.0 
 *  
 *  @section DESCRIPTION
 *  This contains pointers to: 
 *     - directories containing the primary datasets of various sweeps of MCS 
 *     - working directory where to store modified versions of the data
 *  on the Secure Data environment. Other syntax chunks use these paths. 
 *
 *  @section VERSION HISTORY
 *  Date            Author      Version    Revision
 *  25/05/2018      PDM         1.0        Initial commit
 *  13/06/2018      PDM         2.0        Added line for log path
 */

cd "C:\Users\pfdm1m17\git repos\raster_pollution_extraction\MCS syntax"

global pathMCSlong "\\filestore.soton.ac.uk\users\pfdm1m17\mydocuments\0 Datasets\MCS\Longitudinal\stata11"
global pathMCS1 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS1/stata11"
global pathMCS2 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS2/stata11_se"
global pathMCS3 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS3/stata11_se"
global pathMCS4 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS4/stata11_se"
global pathMCS5 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS5/stata11"
global pathMCS6 "//filestore.soton.ac.uk/users/pfdm1m17/mydocuments/0 Datasets/MCS/MCS6/stata11"
global geoMCS1 ""
global geoMCS2 ""
global geoMCS3 ""
global geoMCS4 ""
global geoMCS5 ""
global geoMCS6 ""



*****
* Do not forget to also specify pathOutput in 03_process_address_history.R !!!
*****
global pathOutput "\\filestore.soton.ac.uk\users\pfdm1m17\mydocuments\0 Datasets\batard"

*global pathOutput "\projects$\116964???\Working\data_extracts\"
*global pathLogs "\projects$\116964???\Working\results_logs\"


/*
cd "\projects$\116964???\Working\"
global pathSyntax "\projects$\116964???\Syntax"

global pathMCSlong "Original_data\UKDA-8172-stata11\stata11"
global pathMCS1 "Original_data\UKDA-4683-stata11\stata11"
global pathMCS2 "Original_data\UKDA-5350-stata11_se\stata11_se"
global pathMCS3 "Original_data\UKDA-5795-stata11_se\stata11_se"
global pathMCS4 "Original_data\UKDA-6411-stata11_se\stata11_se"
global pathMCS5 "Original_data\UKDA-7464-stata11\stata11"
global pathMCS6 "Original_data\UKDA-8156-stata11\stata11"
global geoMCS1 "Original_data\sn_7758_MCS_Geog_Sweep1\Stata\7758_MCS_Geog_Sweep1_2nd_Ed_Jan17\stata\stata11"
global geoMCS2 "Original_data\sn_7759_MCS_Geog_Sweep2\Stata\7759_MCS_Geog_Sweep2_2nd_Ed_Jan17\stata\stata11"
global geoMCS3 "Original_data\sn_7760_MCS_Geog_Sweep3\Stata\7760_MCS_Geog_Sweep3_2nd_Ed_Jan17\stata\stata11"
global geoMCS4 "Original_data\sn_7761_MCS_Geog_Sweep4\Stata\7761_MCS_Geog_Sweep4_2nd_Ed_Jan17\stata\stata11"
global geoMCS5 "Original_data\sn_7762_MCS_Geog_Sweep5_2001_Boundaries\Stata\7762_MCS_Geog_Sweep5_2001_Boundaries_2nd_Ed_Jan17\stata\stata11"
global geoMCS6 "Original_data\sn_8231_MCS_Geog_Sweep6_2001_Boundaries\stata\8231_MCS_Geog_Sweep6_2001_Boundaries_1st_Ed_Aug17\stata\stata11"

mcs1_geography_restricted.dta
mcs2_geography_restricted
mcs3_geography_restricted
mcs4_geography_restricted
mcs6_geography2001

*/

* end of script
