/** 
 *  @project MCS Effect of Air Pollution of Cognitive Development
 *  @file    03_generate_address_history.do
 *  @author  Peter Dutey-Magni (PDM)
 *  @email   p.dutey-magni@ucl.ac.uk
 *  @date    13/06/2018
 *  @version 2.0 
 *  
 *  @section DESCRIPTION
 *  This programme looks through subsequent waves of MCS for output area of interview,
 *  the date of interview, and the date of the last move, and saves them into separate
 *  files which can then be picked up by R.
 *
 *  @section VERSION HISTORY
 *  Date            Author      Version    Revision
 *  25/05/2018      PDM         1.0        Initial commit
 *  13/06/2018      PDM         2.0        Added list of assumptions made at line 48 
 *                                         and improved the rules
 */

* Take all output areas ********************************************************
use  `"$geoMCS1/mcs1_geography_restricted "', clear
keep MCSID AOA
merge 1:1 MCSID using `"$geoMCS2/mcs2_geographically_linked_data.dta"'
keep MCSID AOA BOA
merge 1:1 MCSID using `"$geoMCS3/mcs3_geographically_linked_data.dta"'
keep MCSID AOA BOA COA
merge 1:1 MCSID using `"$geoMCS4/mcs4_geographically_linked_data.dta"'
keep MCSID AOA BOA COA DOA
merge 1:1 MCSID using `"$geoMCS5/mcs5_geographically_linked_data.dta"'
keep MCSID AOA BOA COA DOA EOA
merge 1:1 MCSID using `"$geoMCS6/mcs6_geography_2001.dta"'
rename *, lower
keep mcsid aoa boa coa doa eoa foa

*** here reshape wide to long = code not tested, to be tested!!!
reshape long @oa, i(mcsid) j(wave, string)
rename oa OA01CD
rename wave OA

save `"$pathOutput\OA_history.dta"', replace
saveold `"$pathOutput\OA_history_old.dta"', replace version(11)

clear all
set maxvar 10000



* Build a history of successive addresses with dates  **************************

/* Assumptions made in reconstructing address history

-- children exposed to pollution measured at the address of first interview since birth, even if we know
   they have moved home since their birth (reason: we do not know previous address)
   
-- if children move house between waves, a date of move into the current home is recorded during interview. 
   They are assumed to have exposure of their last known address between last productive interview and the
   date of move into current address
   
-- when only month of move is missing but year is know, it is set to June if such a date is coherent with 
   date of last interview and date of current interview... 
   In the opposite case:
         -- if move occured in the year of last interview, month of move is set to 
		      trunc((13 - month of last interview)/2) + 13 - month of last interview
			which is the midpoint between month of last interview and December
		 -- if move occured in the year of the current interview, month of move is set to 
		      trunc(month of current interview/2)
			which is the midpoint between January and month of current interview
*/

use `"$pathMCS1/mcs1_parent_interview.dta"', clear
keep mcsid ahintm00 ahinty00 ammoad00 ammomo00  ahcdbma0 ahcdbya0
gen dob = mdy(ahcdbma0, 15, ahcdbya0)
format %td  dob
drop ahcdbma0 ahcdbya0  

merge 1:1 mcsid using  `"$pathMCS2/mcs2_parent_interview.dta"'
replace dob = mdy(bhcdbma0, 15, bhcdbya0)  if dob == .
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00

merge 1:1 mcsid using  `"$pathMCS3/mcs3_parent_interview.dta"'
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00 chidtm00 chidty00 chadsa00 cmmoad00 cmmomo00   

merge 1:1 mcsid using  `"$pathMCS4/mcs4_parent_interview.dta"'
keep mcsid dob ahintm00 ahinty00 ammoad00 ammomo00  bhidtm00 bhidty00 bmadsa00 bmmoad00 bmmomo00 chidtm00 chidty00 chadsa00 cmmoad00 cmmomo00 dhintm00 dhinty00 dhadsa00 dmmoad00 dmmomo00
save `"$pathOutput\extract_move_dates.dta"', replace

use `"$pathMCS5/mcs5_parent_interview.dta"', clear
rename *, lower
keep mcsid  eelig00 epintm00 epinty00 ehadsa00 epmoad00 epmomo00 
duplicates tag mcsid, gen(duplic_mcsid)
keep if duplic_mcsid == 0 | (duplic_mcsid == 1 & eelig00 == 1)
drop eelig00 duplic_mcsid

merge 1:1 mcsid using `"$pathOutput\extract_move_dates.dta"'
drop _merge
save `"$pathOutput\extract_move_dates.dta"', replace

use `"$pathMCS6/mcs6_parent_interview.dta"', clear
rename *, lower
keep mcsid   felig00  fpinty00 fpintm00 fhadsa00 fpmoad00 fpmomo00 
* ignoring other variables fhmoyry0 fhmoyrm0 which are new but not defined
duplicates tag mcsid, gen(duplic_mcsid)
* if there are several lines of information for a single family, keeping the time of move for the main informant
keep if   duplic_mcsid == 0 | (duplic_mcsid == 1 & felig00 == 1)
drop felig00 duplic_mcsid

merge 1:1 mcsid using `"$pathOutput\extract_move_dates.dta"'
drop _merge
save `"$pathOutput\extract_move_dates.dta"', replace

use `"$pathOutput\extract_move_dates.dta"', clear

* flagging households who refused or do not know when move took place
gen amovmiss = .  
gen bmovmiss = .
gen cmovmiss = .
gen dmovmiss = .
gen emovmiss = .
gen fmovmiss = .

replace amovmiss = 1 if ammoad00 == -8 | ammoad00 == -9
replace bmovmiss = 1 if bmmoad00 == -8 | bmmoad00 == -9
replace cmovmiss = 1 if cmmoad00 == -8 | cmmoad00 == -9
replace dmovmiss = 1 if dmmoad00 == -8 | dmmoad00 == -9
replace emovmiss = 1 if epmoad00 == -8 | epmoad00 == -9
replace fmovmiss = 1 if fpmoad00 == -8 | fpmoad00 == -9

replace amovmiss = 2 if ammomo00 <1900 & ammoad00 > 1900 & ammoad00 < .
replace bmovmiss = 2 if bmmomo00 <1900 & bmmoad00 > 1900 & bmmoad00 < .
replace cmovmiss = 2 if cmmomo00 <1900 & cmmoad00 > 1900 & cmmoad00 < .
replace dmovmiss = 2 if dmmomo00 <1900 & dmmoad00 > 1900 & dmmoad00 < .
replace emovmiss = 2 if epmomo00 <1900 & epmoad00 > 1900 & epmoad00 < .
replace fmovmiss = 2 if fpmomo00 <1900 & fpmoad00 > 1900 & fpmoad00 < .
  
replace amovmiss = 0 if ammomo00 > 0 & ammomo00 < 13 & ammoad00 > 1900
replace bmovmiss = 0 if bmmomo00 > 0 & bmmomo00 < 13 & bmmoad00 > 1900
replace cmovmiss = 0 if cmmomo00 > 0 & cmmomo00 < 13 & cmmoad00 > 1900
replace dmovmiss = 0 if dmmomo00 > 0 & dmmomo00 < 13 & dmmoad00 > 1900
replace emovmiss = 0 if epmomo00 > 0 & epmomo00 < 13 & epmoad00 > 1900
replace fmovmiss = 0 if fpmomo00 > 0 & fpmomo00 < 13 & fpmoad00 > 1900

replace bmovmiss = 3 if bmadsa00 == 1
replace cmovmiss = 3 if chadsa00 == 1
replace dmovmiss = 3 if dhadsa00 == 1
replace emovmiss = 3 if ehadsa00 == 1
replace fmovmiss = 3 if fhadsa00 == 1

label define movmisscodes 1 "Date refused/unknown" 2 "Only month missing" 0 "Date ok" 3 "Didn't move"
label values *movmiss movmisscodes

* Creating date of latest residential move for each wave
gen aint = mdy(ahintm00, 15, ahinty00)
gen bint = mdy(bhidtm00, 15, bhidty00)
gen cint = mdy(chidtm00, 15, chidty00)
gen dint = mdy(dhintm00, 15, dhinty00)
gen eint = mdy(epintm00, 15, epinty00)
gen fint = mdy(fpintm00, 15, fpinty00)

gen amov = mdy(ammomo00, 15, ammoad00)
gen bmov = mdy(bmmomo00, 15, bmmoad00)
gen cmov = mdy(cmmomo00, 15, cmmoad00)
gen dmov = mdy(dmmomo00, 15, dmmoad00)
gen emov = mdy(epmomo00, 15, epmoad00)
gen fmov = mdy(fpmomo00, 15, fpmoad00)
egen datefirstint = rowmin(aint bint cint dint eint fint)

* Impute missing months (only occurs for waves a, c and f and thus only imputing cmov and fmov)

replace cmov = mdy(6, 15, cmmoad00) if cmovmiss == 2 & ( cmmoad00 != chidty00 & cmmoad00 != bhidty00 )
replace cmov = mdy(trunc( chidtm00 / 2 + 0.5), 15, cmmoad00) if cmovmiss == 2 & ( cmmoad00 == chidty00 )
replace cmov = mdy(trunc((13 - bhidtm00) / 2) + bhidtm00, 15, cmmoad00) if cmovmiss == 2 & ( cmmoad00 == bhidty00 )
 
replace fmov = mdy(6, 15, fpmoad00) if fmovmiss == 2 & ( fpmoad00 != fpinty00 & fpmoad00 != epinty00 )
replace fmov = mdy(trunc( fpintm00 / 2 + 0.5), 15, fpmoad00) if fmovmiss == 2 & ( fpmoad00 == fpinty00 )
replace fmov = mdy(trunc((13 - epintm00) / 2) + epintm00, 15, fpmoad00) if fmovmiss == 2 & ( fpmoad00 == epinty00 )

format %td datefirstint aint bint cint dint eint fint amov bmov cmov dmov emov fmov

* Removing dates of move that occurred before date of first interview 
* since no address is known before then
* (note that some of these are not actual moves as much as the interview 
* taking place at another parent's home...)
replace amov = . 
replace bmov = . if bmov < datefirstint  
replace bmov = . if bmov <= datefirstint & aint == .
replace cmov = . if cmov < datefirstint 
replace dmov = . if dmov < datefirstint 
replace emov = . if emov < datefirstint 
replace fmov = . if fmov < datefirstint 


* Removing dates of move that do not fall between last and current interview
* Note: dates of move that are before the last interview date are probably for children with non cohabitating parents
* or other complex circumstances which would be hard to resolve
* THIS IS CURRENTLY NOT ACTIVE - MUST CHECK HOW MUCH SENSE THE POSTCODES MAKE
*replace cmov = . if cmov < bint & cmov != . & bint != .
*replace dmov = . if dmov < cint & dmov != . & cint != .
*replace emov = . if emov < dint & emov != . & dint != .
*replace fmov = . if fmov < eint & emov != .

* Quality assurance
// br if (bmov < aint | bmov > cint) & bmov != .
// br if (cmov < bint | cmov > dint) & cmov != . & bint != .
// br if (dmov < cint | dmov > eint) & dmov != . & cint != .
// br if (emov < dint | emov > fint) & emov != . & dint != .
// br if (fmov < eint ) & fmov != .

keep mcsid bmadsa00 chadsa00 dhadsa00 ehadsa00 fhadsa00 dob aint bint cint dint eint fint amov bmov cmov dmov emov fmov
order mcsid bmadsa00 chadsa00 dhadsa00 ehadsa00 fhadsa00 dob aint bint cint dint eint fint amov bmov cmov dmov emov fmov
save `"$pathOutput\extract_move_dates.dta"', replace
saveold `"$pathOutput\extract_move_dates_old.dta"', replace version(11)
 
* then must switch to R!
clear all
* end of script

