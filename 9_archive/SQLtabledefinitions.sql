

use saturne GO

--drop table [dbo].[OA_2001_OSGB1km_weights]
CREATE TABLE [dbo].[OA_2001_OSGB1km_weights](
	[OA01CD] [char](9) NULL,
	[ukgridcode] [int] NULL,
	[weight] [decimal](22, 20) NULL,
)
GO

CREATE CLUSTERED INDEX IX_OA_2001_OSGB1km_weights_OA01CD
    ON saturne.dbo.OA_2001_OSGB1km_weights
    (OA01CD);

CREATE INDEX IX_OA_2001_OSGB1km_weights_ukgridcode
    ON saturne.dbo.OA_2001_OSGB1km_weights
    (ukgridcode);


--drop TABLE [dbo].[Ricardo_MAAQ_1kmgrid]
CREATE TABLE [dbo].[Ricardo_MAAQ_1kmgrid](
	[ukgridcode] [int] NULL,
	[x] [int] NULL,
	[y] [int] NULL,
	[value] [decimal](22, 9) NULL,
	[year] [smallint] NULL,
	[pollutant] [varchar](45) NULL
);
select top 1000 * from Ricardo_MAAQ_1kmgrid
CREATE CLUSTERED INDEX IX_Ricardo_MAAQ_1kmgrid_ukgridcode
    ON saturne.dbo.Ricardo_MAAQ_1kmgrid
    (ukgridcode);

CREATE INDEX IX_Ricardo_MAAQ_1kmgrid_pollutant
    ON saturne.dbo.Ricardo_MAAQ_1kmgrid
    (pollutant);
CREATE INDEX IX_Ricardo_MAAQ_1kmgrid_year
    ON saturne.dbo.Ricardo_MAAQ_1kmgrid
    (year);

( SELECT OA.oa01cd, RIC.year, RIC.pollutant,
 SUM(OA.weight * RIC.value *10000)/ SUM(OA.weight*10000) AS Mean_concentration, 
 cast(SUM(OA.weight) as decimal(10,10)) AS SW
 into dbo.OA_mean_pollution
 FROM  dbo.[OA_2001_OSGB1km_weights]   OA INNER JOIN
      dbo.Ricardo_MAAQ_1kmgrid   RIC 
 ON OA.ukgridcode = RIC.ukgridcode
GROUP BY OA.oa01cd, RIC.year, RIC.pollutant);
 
 --drop table dbo.OA_mean_pollution
 --CREATE UNIQUE CLUSTERED INDEX IX_V_OA_mean_pollution
 --   ON saturne.dbo.V_OA_mean_pollution
 --   (OA01CD );
