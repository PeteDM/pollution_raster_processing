# Description #

This repository contains several projects related to the analysis of ambient pollution and its effects on pupil attainment (National Pupil Database) and cognitive development (Millenium Cohort Study)

Click here to see the source content of the repository:

[Source](https://bitbucket.org/PeteDM/raster_pollution_extraction/src)

### Who do I talk to? ###

Peter Dutey-Magni

[p.dutey-magni@ucl.ac.uk](mailto:p.dutey-magni@ucl.ac.uk)